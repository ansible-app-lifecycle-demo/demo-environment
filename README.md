# demo-environment

Ansible APP Lifecycle Demo

## Demo/Lab Developers:

*Carlos Lopez Bartolome*, Specialist Solution Architect, Red Hat

*Cesar Fernandez*, EMEA Ansible Specialist Solution Architect, Red Hat

## Prerequisites

- Slack App.
- OpenShift Cluster +4.12 with admin rights.
- Ansible Navigator.
- Demo Environment project.

### Slack App

You can create a Slack App in the following [link](https://aapdemo.slack.com/apps).

### OpenShift

If you don't have an OpenShift Cluster 4.12 with admin rights, you can request one in the following [link](https://demo.redhat.com/catalog?category=Workshops&item=babylon-catalog-prod%2Fsandboxes-gpte.ocp412-wksp.prod).

### Ansible Navigator

- If you have Linux, you can install the `ansible-navigator` tool following the steps described in the following [link](https://ansible-navigator.readthedocs.io/installation/#linux).

> Note: Run the command `ansible-navigator --version` to make sure it was installed correctly.

- After installing it, create the `inventory` and the `ansible-navigator` definition file:

```sh
cd ~
mkdir ansible-navigator
cat << EOF > ansible-navigator/inventory
[controller]
localhost ansible_connection=local
EOF
cat << EOF > ansible-navigator/ansible-navigator.yml
---
ansible-navigator:
  ansible:
    inventory:
      entries:
      - ./inventory
  app: run
  editor:
    command: vim_from_setting
    console: false
  execution-environment:
    container-engine: podman
    image: registry.redhat.io/ansible-automation-platform-23/ee-supported-rhel8:latest
    pull:
      policy: missing
  logging:
    append: true
    file: /tmp/navigator/ansible-navigator.log
    level: debug
  playbook-artifact:
    enable: false
EOF
```

## Demo Environment Project

- Clone the demo environment project:

```sh
cd ~
git clone https://gitlab.com/ansible-app-lifecycle-demo/demo-environment.git
```

## Demo Environment Deployment

This is the demo topology that we are going to deploy:

![topology](images/demo-env.png)

Follow the steps described bellow to deploy the demo environment:

- Create the demo configuration variables file, replacing the highlighted variables:

```sh
cat << EOF > demo-environment/vars/demo-config.yml
---
# Demo vars
app_lifecycle_demo_namespace: app-lifecycle-demo
app_lifecycle_demo_templates_path: templates

# OCP vars
ocp_templates_path: "{{ app_lifecycle_demo_templates_path }}/images"

# Gitea vars
gitea_templates_path: "{{ app_lifecycle_demo_templates_path }}/gitea"

# Nexus vars
nexus_templates_path: "{{ app_lifecycle_demo_templates_path }}/nexus"

# ArgoCD vars
argocd_operator_templates_path: "{{ app_lifecycle_demo_templates_path }}/argocd/operator"
argocd_instance_templates_path: "{{ app_lifecycle_demo_templates_path }}/argocd/instance"
argocd_instance_admin_pasword: redhat
argocd_config_templates_path: "{{ app_lifecycle_demo_templates_path }}/argocd/config"

# Slack vars
slack_token: "<SLACK_TOKEN>"
slack_channel: "#<SLACK_CHANNEL>"

# Ansible vars
ansible_operator_templates_path: "{{ app_lifecycle_demo_templates_path }}/ansible/operator"
ansible_instance_templates_path: "{{ app_lifecycle_demo_templates_path }}/ansible/instance"
ansible_instance_admin_password: redhat
ansible_instance_manifest: "<AAP_MANIFEST>" --> !!!ENCODE IN BASE64!!!
EOF
```

- Deploy the demo environment:

```sh
cd ~/ansible-navigator
ansible-navigator run ../demo-environment/ocp-demo-deploy.yml -m stdout \
  -e 'ansible_python_interpreter=/usr/bin/python3' \
  -e 'openshift_api=<OPENSHIFT_URL>' \
  -e 'openshift_token=<OPENSHIFT_API>' \
  -e 'openshift_storage_class=<OPENSHIFT_STORAGE_CLASS>'
```
